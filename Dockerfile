FROM openjdk:21-jdk-bullseye

LABEL maintainer="haath@protonmail.com"

# obtain at: http://d.defold.com/stable/info.json
ENV DEFOLD_VERSION 1.9.6
ENV DEFOLD_VERSION_SHA1 11d2cd3a9be17b2fc5a2cb5cea59bbfb4af1ca96

# copy shortcut scripts
COPY ./scripts/ /usr/local/bin/
RUN chmod +x /usr/local/bin/*

# download bob.jar
RUN curl -L -o /usr/local/bin/bob.jar http://d.defold.com/archive/${DEFOLD_VERSION_SHA1}/bob/bob.jar

# download dmengine_headless
RUN curl -L -o /usr/local/bin/dmengine_headless http://d.defold.com/archive/${DEFOLD_VERSION_SHA1}/engine/x86_64-linux/dmengine_headless \
    && chmod +x /usr/local/bin/dmengine_headless

# update file permissions
RUN chown root:root /usr/local/bin/*

# install some dependencies
# according to: https://forum.defold.com/t/spine-4-1/72923/2
RUN apt-get update \
 && apt-get install -y --no-install-recommends libopenal-dev libgl1-mesa-dev libglw1-mesa-dev freeglut3-dev \
 && apt-get autoremove -y \
 && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

CMD [ "bob" ]
