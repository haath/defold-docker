<div align="center">

![](https://gitlab.com/haath/defold-docker/-/raw/master/logo.png)

[![](https://img.shields.io/docker/v/gmantaos/defold?sort=semver)](https://hub.docker.com/r/gmantaos/defold)
[![](https://img.shields.io/docker/pulls/gmantaos/defold.svg)](https://hub.docker.com/r/gmantaos/defold)
[![](https://img.shields.io/docker/image-size/gmantaos/defold?sort=semver)](https://hub.docker.com/r/gmantaos/defold)

</div>

Simple image on top of `openjdk:17-jdk-bullseye`, with the following two executables present in PATH:

- bob
- dmengine_headless


## Usage

Intended use is for CI build environments.

```yml
image: gmantaos/defold

build:
  script:
  - bob --debug build
```


## Env

| Variable | Value |
| -------- | ----- |
| DEFOLD_VERSION | `1.9.6` |
| DEFOLD_VERSION_SHA1 | `11d2cd3a9be17b2fc5a2cb5cea59bbfb4af1ca96` |
